package com.example.FirstTask.Models;


import com.fasterxml.jackson.annotation.JsonIgnore;
import javax.persistence.*;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name="movie_part")
public class MoviePart {

    @ManyToOne
    @JoinColumn(name = "movie_Id")//name="the name in the Movie table" y3ni osom il foreign key illi sar 3ndo mn il table illi ma3o relationship
    @JsonIgnore
    private Movie movie;

    @OneToMany(mappedBy = "moviePart")
    private List<Comment> comments;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long moviePartId;

    @Column(name="moviePartName")
    private String moviePartName;

    @Column(name="moviePartRate")
    private double moviePartRate;

    @Column(name="moviePartNumber")
    private int moviePartNumber ;

    public Movie getMovie() {
        return movie;
    }

    public void setMovie(Movie movie) {
        this.movie = movie;
    }

    public List<Comment> getComments() {
        return comments;
    }

    public void setComments(List<Comment> comments) {
        this.comments = comments;
    }

    public MoviePart(){
    }

    public MoviePart(String moviePartName, double moviePartRate, int moviePartNumber){
        this.moviePartName=moviePartName;
        this.moviePartRate=moviePartRate;
        this.moviePartNumber=moviePartNumber;
    }

    public long getMoviePartId() {
        return moviePartId;
    }

    public void setMoviePartId(long moviePartId) {
        this.moviePartId = moviePartId;
    }

    public String getMoviePartName() {
        return moviePartName;
    }

    public void setMoviePartName(String moviePartName) {
        this.moviePartName = moviePartName;
    }

    public double getMoviePartRate() {
        return moviePartRate;
    }

    public void setMoviePartRate(double moviePartRate) {
        this.moviePartRate = moviePartRate;
    }

    public int getMoviePartNumber() {
        return moviePartNumber;
    }

    public void setMoviePartNumber(int moviePartNumber) {
        this.moviePartNumber = moviePartNumber;
    }

    @Override
    public String toString() {
        return "MoviePart{" +
                "movie=" + movie +
                ", comments=" + comments +
                ", moviePartId=" + moviePartId +
                ", moviePartName='" + moviePartName + '\'' +
                ", moviePartRate=" + moviePartRate +
                ", moviePartNumber=" + moviePartNumber +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MoviePart moviePart = (MoviePart) o;
        return moviePartId == moviePart.moviePartId && Double.compare(moviePart.moviePartRate, moviePartRate) == 0 && moviePartNumber == moviePart.moviePartNumber && Objects.equals(movie, moviePart.movie) && Objects.equals(comments, moviePart.comments) && Objects.equals(moviePartName, moviePart.moviePartName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(movie, comments, moviePartId, moviePartName, moviePartRate, moviePartNumber);
    }

}
