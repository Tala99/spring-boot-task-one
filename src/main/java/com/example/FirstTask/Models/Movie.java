package com.example.FirstTask.Models;

import javax.persistence.*;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name="movie")
public class Movie {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name="movie_Id")
    private long id;

    @Column(name="movie_name")
    private String name;

    @Column(name="movie_type")
    private String type;

    @OneToMany(mappedBy="movie")
    private List<MoviePart> parts;

    public List<MoviePart> getParts() {
        return parts;
    }

    public void setParts(List<MoviePart> parts) {
        this.parts = parts;
    }

    public Movie() {
    }

    public Movie(String name, String type) {
        this.name=name;
        this.type=type;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return "Movie{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", type='" + type + '\'' +
                ", parts=" + parts +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Movie movie = (Movie) o;
        return id == movie.id && Objects.equals(name, movie.name) && Objects.equals(type, movie.type) && Objects.equals(parts, movie.parts);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, type, parts);
    }

}
