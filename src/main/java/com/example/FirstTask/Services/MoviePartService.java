package com.example.FirstTask.Services;

import com.example.FirstTask.Models.Movie;
import com.example.FirstTask.Models.MoviePart;
import com.example.FirstTask.Repositories.MoviePartRepository;
import com.example.FirstTask.Repositories.MovieRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class MoviePartService {

    @Autowired
    MoviePartRepository moviePartRepository;

    @Autowired
    MovieRepository movieRepository;

    public List<MoviePart> getAllMovieParts() {
        return moviePartRepository.findAll();
    }

    public void createMoviePart(long movie_id, MoviePart moviePart) {
        Optional<Movie> movie = movieRepository.findById(movie_id);
        if(movie.isPresent()) {
            moviePart.setMovie(movie.get());
                               }
        moviePartRepository.save(moviePart);
    }

    public void updateMoviePart(MoviePart moviePart) {
        try {
            long id = moviePart.getMoviePartId();
            if (moviePartRepository.findById(id).isPresent()) {
                moviePartRepository.save(moviePart);
            }
        }
        catch (Exception e) {
            throw e;
        }
    }

    //public void updateMoviePartRate(MoviePart moviePart) {
    //   MoviePart rate = moviePartRepository.findById(moviePart.getMoviePartId())
    //}

    public void deleteMoviePart(long moviePartId) {
        moviePartRepository.deleteById(moviePartId);
    }

}
