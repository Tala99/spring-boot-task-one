package com.example.FirstTask.Services;

import com.example.FirstTask.Models.Comment;
import com.example.FirstTask.Models.MoviePart;
import com.example.FirstTask.Repositories.CommentRepository;
import com.example.FirstTask.Repositories.MoviePartRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class CommentService {

    @Autowired
    CommentRepository commentRepository;

    @Autowired
    MoviePartRepository moviePartRepository;

    public List<Comment> getAllComments() {
        return commentRepository.findAll();
    }

    public void createComment(long moviePartId, Comment comment) {
        Optional<MoviePart> moviePart = moviePartRepository.findById(moviePartId);
        if(moviePart.isPresent()) {
            comment.setMoviePart(moviePart.get());
        }
        commentRepository.save(comment);
    }

    public void updateComment(Comment comment) {
        try {
            long id = comment.getCommentId();
            if (commentRepository.findById(id).isPresent()) {  //Can we say if (id != null) instead ?
                commentRepository.save(comment);
            }
        }
        catch (Exception e) {
            throw e;
        }
    }

    public void deleteComment(long comment_id) {
        commentRepository.deleteById(comment_id);
    }

}
