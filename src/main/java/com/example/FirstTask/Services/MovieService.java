package com.example.FirstTask.Services;

import com.example.FirstTask.Models.Movie;
import com.example.FirstTask.Models.MoviePart;
import com.example.FirstTask.Repositories.MoviePartRepository;
import com.example.FirstTask.Repositories.MovieRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class MovieService {

    @Autowired
    MovieRepository movieRepository;

    @Autowired
    MoviePartRepository moviePartRepository;

    public List<Movie> getAllMovies() {
        return movieRepository.findAll();
    }

    public Optional<Movie> getMovieById(long id) {
        return movieRepository.findById(id);
    }

    public void setMovie(Movie movie) {
        movieRepository.save(movie);
    }

    public void updateMovie(Movie movie) {
        try {
            long id = movie.getId();
            if(movieRepository.findById(id).isPresent())
                movieRepository.save(movie);
        }
        catch (Exception e) {
            throw e;
        }
    }

    public void deleteMovieById(long id) {
        movieRepository.deleteById(id);
    }

    //public void createMoviePart(long movieId, MoviePart moviePart) {
      //  if(movieRepository.findById(movieId).isPresent()){
       // }
    //}

}

