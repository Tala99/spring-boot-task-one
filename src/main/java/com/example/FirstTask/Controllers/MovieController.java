package com.example.FirstTask.Controllers;

import com.example.FirstTask.Models.Movie;
import com.example.FirstTask.Models.MoviePart;
import com.example.FirstTask.Services.MoviePartService;
import com.example.FirstTask.Services.MovieService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/movies")
public class MovieController {

    @Autowired
    MovieService movieService;

    @GetMapping("")
    public List<Movie> getAllMovies() {
        return movieService.getAllMovies();
    }

    @GetMapping("/{id}")
    public Optional<Movie> getMovieById(@PathVariable long id) {
        return movieService.getMovieById(id);
    }

    @PostMapping("")
    public void createMovie(@RequestBody Movie movie) {
        movieService.setMovie(movie);
    }

//    @PostMapping("/{id}") // Add Movie_Parts to a certain Movie
//    public void createMoviePart(@PathVariable long movieId, @RequestBody MoviePart moviePart) {
//        movieService.createMoviePart(movieId, moviePart);
//    }

    @PutMapping("")
    public void updateMovie(@RequestBody Movie movie) {
        movieService.updateMovie(movie);
    }

    @DeleteMapping("/{id}")
    public void deleteMovieById(@PathVariable long id) {
        movieService.deleteMovieById(id);
    }

}
