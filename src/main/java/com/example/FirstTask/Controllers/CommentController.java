package com.example.FirstTask.Controllers;

import com.example.FirstTask.Models.Comment;
import com.example.FirstTask.Services.CommentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/comments")
public class CommentController {

    @Autowired
    CommentService commentService;

    @GetMapping("")
    public List<Comment> getAllComments() {
        return commentService.getAllComments();
    }

    @PostMapping("/{moviePartId}")
    public void createComment(@PathVariable long moviePartId, @RequestBody Comment comment) {
        commentService.createComment(moviePartId,comment);
    }

    @PutMapping("/{comment_Id}")
    public void updateComment(@RequestBody Comment comment) {
        commentService.updateComment(comment);
    }

    @DeleteMapping("/{comment_Id}")
    public void deleteComment(@PathVariable long comment_Id) {
        commentService.deleteComment(comment_Id);
    }

}
