package com.example.FirstTask.Controllers;

import com.example.FirstTask.Models.Movie;
import com.example.FirstTask.Models.MoviePart;
import com.example.FirstTask.Repositories.MovieRepository;
import com.example.FirstTask.Services.MoviePartService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/movie_parts")
public class MoviePartController {

    @Autowired
    MoviePartService moviePartService;

    @GetMapping("")
    public List<MoviePart> getAllMovieParts() {
       return moviePartService.getAllMovieParts();
    }

    @PostMapping("/{movie_id}") // To add Movie_Parts to a certain Movie
    public void createMoviePart(@PathVariable long movie_id, @RequestBody MoviePart moviePart) {
      moviePartService.createMoviePart(movie_id,moviePart);
    }

    @PutMapping("/{moviePartId}")
    public void updateMoviePart(@RequestBody MoviePart moviePart) {
        moviePartService.updateMoviePart(moviePart);
    }

//    @PutMapping("/{moviePartRate}")
//    public void updateMoviePartRate(@RequestBody MoviePart moviePart ){
//        moviePartService.updateMoviePartRate(moviePart);
//    }

    @DeleteMapping("/{moviePartId}")
    public void deleteMoviePart(@PathVariable long moviePartId) {
        moviePartService.deleteMoviePart(moviePartId);
    }

}
